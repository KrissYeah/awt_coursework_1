from modules import app
from modules import filereader as freader
from modules.datamodel import albumlist

if __name__ == "__main__":
  
  print ("**** READ CSV DATABASE ****")
  ### read database files from CSV
  freader.readFile()
  print ("**** FINISHED READING CSV DATABASE ****")
  
  print ("**** START SERVER ****")
  ### user reloader avoid running server twice in debug mode^
  app.run(host='0.0.0.0',debug=True, use_reloader=False)

