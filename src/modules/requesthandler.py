from modules import app
from flask import Flask, render_template, url_for, request, redirect, abort
from modules.datamodel import albumlist as list
import modules.globalmember as gl

# main route
@app.route('/')
def index():
  return showList()

# basic list view
@app.route('/list/')
def showList():
  if (gl.DEBUGMODE_PRINT):
    print "** listView request"
  return render_template('listview.html', albumlist=list)

# basic position detail view
@app.route('/list/pos/<int:index>/')
def showslide(index):
  return render_template('detailview.html', album=list[index-1])

# display all albums for specific year
@app.route('/list/year/<int:x>/')
def showalbumsfromyear(x):
  output = []
  for album in list:
    if (album.year == x):
      output.append(album)
  return render_template('listview.html', albumlist=output)

# display all albums in a specific year range
@app.route('/list/year/<int:x>/<int:y>/')
def showalbumsfromyeartoyear(x, y):
  output = []
  for album in list:
    if (album.year >= x and album.year <= y):
      output.append(album)
  return render_template('listview.html', albumlist=output)

# display all albums in a specific position range
@app.route('/list/pos/<int:x>/<int:y>/')
def showalbumsfrompostopos(x, y):
  output = []
  for album in list:
    if (album.pos >= x and album.pos <= y):
      output.append(album)
  return render_template('listview.html', albumlist=output)

# display all albums with specifiv artist
@app.route('/list/artist/<string:artist>/')
def showalbumsfromartist(artist):
  output = []
  for album in list:
    if(album.artist == artist):
      output.append(album)
  return render_template('listview.html', albumlist=output)

### filter all input data from Form
# herarchiy position > year > artist
@app.route('/list/filter/', methods=['POST','GET'])
def filter():
  if request.method == 'POST':
    fromPos = request.form['frompos']
    toPos = request.form['topos']
    fromyear = request.form['fromyear']
    toyear = request.form['toyear']
    name = request.form['searchfield']

    output = []

    ### cast numbers if string is not empty
    if (fromPos != ""): fromPos = int(fromPos)
    if (toPos != ""): toPos = int(toPos)
    if (fromyear != ""): fromyear = int(fromyear)
    if (toyear != ""): toyear = int(toyear)

    currentLen = len(list)

    ### if klick on filter button but no input was made
    if (fromPos == "" and toPos == "" and fromyear == "" and toyear == "" and name == ""):
      return redirect(url_for('showList'))
    
    # from pos to pos only AND
    if (fromPos != "" and toPos != "" and fromyear == "" and toyear == "" and name == ""):
      return redirect(url_for('showalbumsfrompostopos', x=fromPos, y=toPos))
    
    # from or to pos only OR
    if ((fromPos != "" or toPos) != "" and fromyear == "" and toyear == "" and name == ""):
      if (fromPos != ""): return redirect(url_for('showalbumsfrompostopos', x=fromPos,y=currentLen))
      else: return redirect(url_for('showalbumsfrompostopos', x=1, y=toPos))
    
    # from year to year only AND
    if (fromPos == "" and toPos == "" and fromyear != "" and toyear != "" and name == ""):
      return redirect(url_for('showalbumsfromyeartoyear', x=fromyear, y=toyear))
    
    # from year or to year only OR
    if (fromPos == "" and toPos == "" and (fromyear != "" or toyear) != "" and name == ""):
      if (fromyear != ""): return redirect(url_for('showalbumsfromyeartoyear', x=fromyear, y=2100))
      else: return redirect(url_for('showalbumsfromyeartoyear', x=1900, y=toyear))
    
   # if (fromPos == "" and toPos == "" and fromyear == "" and toyear == "" and name == ""):
    ## add artist

    if (fromPos == ""): fromPos = 1
    if (toPos == ""): toPos = currentLen
    if (fromyear == ""): fromyear = 1900
    if (toyear == ""): toyear = 2100
    ### for development because was using shorter list
    if (currentLen < toPos):
      toPos = currentLen

    for i in range(fromPos-1, toPos-1):
      if(list[i].year >= fromyear and list[i].year <= toyear):
        if (name == ""):
          output.append(list[i])
        elif (name.lower() in list[i].artist.lower()):
          output.append(list[i])
  
    return render_template('listview.html', albumlist=output)
  abort(404)
#### ERROR HANDLER ####
@app.errorhandler(404)
def page_not_found(error):
  return render_template('errorpage.html'), 404
